// eslint-disable-next-line no-unused-vars
import React from 'react';
import './App.css';
// eslint-disable-next-line no-unused-vars
import Timeclock from './containers/Timeclock/Timeclock';
// eslint-disable-next-line no-unused-vars
import {BrowserRouter} from 'react-router-dom'

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Timeclock />
      </div>
    </BrowserRouter>
  );
}

export default App;
