import * as actionTypes from './actions';

const initialState = {
    isAuth: false,
    id:0
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.LOGIN:
            return{
                ...state,
                isAuth:true,
                id:action.userData.userId
            }
        case actionTypes.LOGOUT:
            return {
                ...state,
                isAuth:false,
                id:0
            }
        default:
            break;
    }

    return state;
};

export default reducer;