import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://timeclock-a4b2e.firebaseio.com/'
});

instance.defaults.headers.common['Authorization'] = 'AIzaSyAlYTeVGBxE4CtqsdPF7xJrl9snMa_dVz0';

export default instance;