// eslint-disable-next-line no-unused-vars
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// eslint-disable-next-line no-unused-vars
import App from './App';
import * as serviceWorker from './serviceWorker';

// Importing the Bulma CSS library
import 'bootstrap/dist/css/bootstrap.css';
// eslint-disable-next-line no-unused-vars
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import StateLoader from './store/stateLoader'
import reducer from './store/reducer';

const stateLoader = new StateLoader();
const store = createStore(reducer, stateLoader.loadState());
store.subscribe(() => {
    stateLoader.saveState(store.getState())
})

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
