import React, {useRef} from 'react'
import classes from './Auth.module.css'
import { connect } from 'react-redux'
import * as actionTypes from '../../store/actions'
import Axios from '../../services/axios'

const Auth = (props) => {
    let user = useRef(null)
    let pass = useRef(null)

    const LoginHandler = () => {
        if(user.current.value && pass.current.value){
            Axios.get('/Users.json')
                .then(response => {
                    for (let users in response.data){
                        if ((response.data[users].name === user.current.value) && (response.data[users].password === pass.current.value)){
                            props.onLogin(response.data[users].id)                      
                        }
                    }
                })
        }
    }

    return (
        <div className="container">
            <form className="form-group ">
            <div className="justify-content-center">
                <div className={classes.AuthForm}>
                    <input ref={user} className='col col-4 form-control' type="text" placeholder="Username" required></input>
                    <input ref={pass} className='col col-4 form-control' type="password" placeholder="Password" required></input>
                    <button type="button" onClick={LoginHandler} className="btn btn-primary">Login</button>
                </div>
            </div>
            </form>
        </div>
    )
}

const mapStatetoProps = state => {
    return {
        auth: state.isAuth
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onLogin: (id) => dispatch({type: actionTypes.LOGIN, userData:{userId:id}})
    }
};

export default connect(mapStatetoProps, mapDispatchToProps)(Auth);