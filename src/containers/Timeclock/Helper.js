export const getAccrued = (time1, time2, time3, time4) => {
    let hSta1 = time1.split(':'); 
    let hFin1 = time2.split(':'); 
    let hSta2 = time3.split(':'); 
    let hFin2 = time4.split(':'); 
    let horasTotal = ((parseInt(hFin1[0], 10) - parseInt(hSta1[0], 10)) + (parseInt(hFin2[0], 10) - parseInt(hSta2[0], 10)) - 8); 
    let minutosTotal = (parseInt(hFin1[1], 10) - parseInt(hSta1[1], 10)) + (parseInt(hFin2[1], 10) - parseInt(hSta2[1], 10)); 
    if(minutosTotal < 0 && horasTotal > 0)
    { 
        minutosTotal += 60; 
        horasTotal -= 1; 
    } 
    else if (horasTotal === 0 && minutosTotal < 0)
    {
        minutosTotal *=-1
        horasTotal = String('-' + String(horasTotal).padStart(2,'0'))
    }
    return String(horasTotal).padStart(2, '0') + ":" + String(minutosTotal).padStart(2,'0'); 
}

export const validateTime = (timeInput, timeAfter) => {
    let hInput1 = timeInput.split(':'); 
    let hAfter1 = timeAfter.split(':'); 
    return ((parseInt(hInput1[0], 10) > parseInt(hAfter1[0], 10)) || ((parseInt(hInput1[0], 10) === parseInt(hAfter1[0], 10)) 
                                                               && (parseInt(hInput1[1], 10) > parseInt(hAfter1[1], 10))))
}

export const sumAccrued = (time1, time2) => {
    if (time1 && (time1 !== '00:00')){
        if((String(time1).search("-") >= 0) && (String(time2).search("-") >=0 ))
        {
            //Sum of negative hours
            let hSta1 = String(time1).trim("-").split(':'); 
            let hFin1 = String(time2).trim("-").split(':'); 
            let horasTotal = ((parseInt(hFin1[0], 10) + parseInt(hSta1[0], 10))); 
            let minutosTotal = ((parseInt(hFin1[1], 10) + parseInt(hSta1[1], 10))); 
            if(minutosTotal > 60)
            {  
                minutosTotal -= 60; 
                horasTotal += 1; 
            }
            return String('-' + horasTotal).padStart(2, '0') + ":" + String(minutosTotal).padStart(2,'0'); 
        }
        else if ((String(time1).search("-") >=0) && ((String(time2).search("-")) < 0)) {
            //Sum when first is negative and the second isn't
            let hSta1 = String(time1).trim("-").split(':'); 
            let hFin1 = String(time2).split(':'); 
            let horasTotal = ((parseInt(hFin1[0], 10)) - (parseInt(hSta1[0], 10))); 
            let minutosTotal = ((parseInt(hFin1[1], 10)) - (parseInt(hSta1[1], 10)));  
            if(minutosTotal < 0 && horasTotal > 0)
            {  
                minutosTotal += 60; 
                horasTotal -=1;
            }
            else if (minutosTotal < 0){
                minutosTotal *=-1
                horasTotal = String('-' + String(horasTotal).padStart(2,'0'))
            }
            return String(horasTotal).padStart(2, '0') + ":" + String(minutosTotal).padStart(2,'0'); 
        } 
        else if ((String(time1).search("-")<0) && (String(time2).search("-") >= 0)) {
            //Sum when second is negative and the first isn't
            let hSta1 = String(time1).split(':'); 
            let hFin1 =String(time2).trim("-").split(':'); 
            let horasTotal = ((parseInt(hSta1[0], 10) - (parseInt(hFin1[0], 10)))); 
            let minutosTotal = ((parseInt(hSta1[1], 10) - (parseInt(hFin1[1], 10)))); 
            if(minutosTotal < 0 && horasTotal > 0)
            {  
                minutosTotal += 60; 
                horasTotal -=1;
            }
            else if (minutosTotal < 0){
                minutosTotal *=-1
                horasTotal = String('-' + String(horasTotal).padStart(2,'0'))
            }
            return String(horasTotal).padStart(2, '0') + ":" + String(minutosTotal).padStart(2,'0'); 
        }
        else {
            //Sum of hours default
            let hSta1 = String(time1).split(':'); 
            let hFin1 = String(time2).split(':'); 
            let horasTotal = ((parseInt(hFin1[0], 10) + parseInt(hSta1[0], 10))); 
            let minutosTotal = ((parseInt(hFin1[1], 10) + parseInt(hSta1[1], 10))); 
            if(minutosTotal > 60)
            {  
                minutosTotal -= 60; horasTotal += 1; 
            }
            return String(horasTotal).padStart(2, '0') + ":" + String(minutosTotal).padStart(2,'0'); 
        }     
    }
    else{
        return time2;
    }
}

