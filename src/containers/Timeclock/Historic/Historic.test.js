import React from 'react'
import { configure, shallow } from 'enzyme'
import EnzymeAdapter from 'enzyme-adapter-react-16'
import HistoricRegisters from '../../../components/HistoricRegisters/HistoricRegisters'

configure({adapter: new EnzymeAdapter()})

describe('<HistoricRegistes />', () => {
    const wrapper = shallow(<HistoricRegisters registers={[]} ></HistoricRegisters>);
    it("It should render the <HistoriRegisters/>", 
    () => {
        expect(wrapper.find(HistoricRegisters));
    });

    it("It should render a form", 
    () => {
        expect(wrapper.find("form"));
    });
 
}) 