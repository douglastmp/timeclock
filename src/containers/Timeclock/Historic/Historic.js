import React, {useMemo, useState, useEffect, useRef, useContext} from 'react'
import classes from './Historic.module.css'
import HistoricRegisters from '../../../components/HistoricRegisters/HistoricRegisters'
import Axios from '../../../services/axios'
import DateHelper from 'date-fns';
import { sumAccrued } from '../Helper';
import UserContext from '../User/UserContext'


const Historic = ( props ) => {
    const [data, setData] = useState([]);
    let responseData = [];
    let result = ''
    const initialDate = useRef();
    const finalDate = useRef();
    const userContext = useContext(UserContext)
    
    useEffect(() => {
        Axios.get('/Timeregister.json')
        .then((response) => {
            for(let register in response.data){
                console.log(userContext.id)
                if (DateHelper.isThisMonth(new Date(response.data[register].day)) && (userContext.id === response.data[register].user)) {                    
                    // eslint-disable-next-line react-hooks/exhaustive-deps
                    result = sumAccrued(result, response.data[register].accrued)
                    responseData.push({id:register, 
                                    date:DateHelper.format(new Date(response.data[register].day), 'DD/MM/YYYY'), 
                                    time1:response.data[register].time1, 
                                    time2:response.data[register].time2,
                                    time3:response.data[register].time3,
                                    time4:response.data[register].time4,
                                    accrued:response.data[register].accrued,
                                    accumulated:result
                                });
                    }
            }
            setData(responseData)
        })
        }, [])


    const FilterDataHandler= (() => {
        Axios.get('/Timeregister.json')
        .then((response) => {
            if ((initialDate.current.value) && (finalDate.current.value) && (DateHelper.isBefore(initialDate.current.value, finalDate.current.value) || DateHelper.isSameDay(initialDate.current.value, finalDate.current.value)))
            {
                for(let register in response.data ){    
                    if(response.data[register].user === userContext.id){
                        if (((DateHelper.isBefore(new Date(response.data[register].day),  finalDate.current.value)) 
                        &&   (DateHelper.isAfter(new Date(response.data[register].day),   initialDate.current.value)))
                        ||  ((DateHelper.isSameDay(new Date(response.data[register].day), initialDate.current.value))
                        ||   (DateHelper.isSameDay(new Date(response.data[register].day), finalDate.current.value))) )  {                    
                            result = sumAccrued(result, response.data[register].accrued)
                            responseData.push({id:register, 
                                            date:DateHelper.format(new Date(response.data[register].day), 'DD/MM/YYYY'), 
                                            time1:response.data[register].time1, 
                                            time2:response.data[register].time2,
                                            time3:response.data[register].time3,
                                            time4:response.data[register].time4,
                                            accrued:response.data[register].accrued,
                                            accumulated:result
                                        });
                            }
                        } 
                }
            } else 
            {
                for(let register in response.data){
                    if (DateHelper.isThisMonth(new Date(response.data[register].day)) && (response.data[register].user === userContext.id)) {                    
                        result = sumAccrued(result, response.data[register].accrued)
                        responseData.push({id:register, 
                                        date:DateHelper.format(new Date(response.data[register].day), 'DD/MM/YYYY'), 
                                        time1:response.data[register].time1, 
                                        time2:response.data[register].time2,
                                        time3:response.data[register].time3,
                                        time4:response.data[register].time4,
                                        accrued:response.data[register].accrued,
                                        accumulated:result
                                    });
                        }
                }                
            }
            setData(responseData)
        })
    })


    return(        
        <React.Fragment>
        <div className={classes.Title}>
            <h2>Accumulated hours of the Month</h2>
        </div>            
        <div>
            {useMemo(() => <HistoricRegisters registers={data}/>, [data])}
        </div>
        <div className="container">
            <h4>Choose the range to filter</h4>
            <form>
                <div className="d-flex flex-row bd-highlight mb-3">
                    <div className="p-2 bd-highlight form-group col-6 float-left">
                        <label>Initial Date:</label>
                        <input ref={initialDate} type="date" className="form-control" max="9999-12-31"></input>
                    </div>
                    <div className="p-2 bd-highlight form-group col-6 float-right">
                        <label >Final Date:</label>
                        <input ref={finalDate} type="date" className="form-control" max="9999-12-31"></input>
                    </div>
                </div>
                <div className="container">
                    <div className="form-group form-check float-none">
                        <button type="button" className="btn btn-primary" onClick={FilterDataHandler}>Filter</button>        
                    </div>
                </div>
            </form>
        </div>
        </React.Fragment>
    );
}

export default Historic