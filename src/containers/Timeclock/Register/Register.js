import React, {useRef, useMemo, useEffect, useState, useContext} from 'react';
import classes from './Register.module.css';
import TimeRegister from '../../../components/TimeRegister/TimeRegister';
import Axios from '../../../services/axios'
import {getAccrued, validateTime, sumAccrued} from '../Helper';
import DateHelper from 'date-fns';
import UserContext from '../User/UserContext'

const Register = (props) => {
    const timeValue = useRef(null);
    const daysOfWeek = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    const [registers, setRegisters] = useState([{}])
    const [warningMessage, setWarningMessage] = useState();
    const [currentRegister, setCurrentRegister] = useState();
    const [weekResult, setWeekResult] = useState();
    const [minTime, setMinTime] = useState();
    let urlPut = '';
    let result = '00:00';
    let lastTime = '';
    let timeRegisterData = {};
    const arrayData = [];
    const userContext = useContext(UserContext)

    useEffect(() => {
    Axios.get('/Timeregister.json')
    .then((response) => {
        for(let register in response.data ){
            if (DateHelper.isThisWeek(new Date(response.data[register].day)) && (response.data[register].user === userContext.id)) {
                arrayData.push({id:register, day:response.data[register].dayweek, 
                                time1:response.data[register].time1, 
                                time2:response.data[register].time2,
                                time3:response.data[register].time3,
                                time4:response.data[register].time4,
                                accrued:response.data[register].accrued});
                if (response.data[register].accrued){
                    // eslint-disable-next-line react-hooks/exhaustive-deps
                    result = sumAccrued(result, response.data[register].accrued);
                }                          
            }

        }
        if(arrayData.length > 0){
            if (!arrayData[arrayData.length - 1].time2) {
                setCurrentRegister(<h1 className={classes.register}>Register your lunch break time exit!</h1>)
            }
            else if (!arrayData[arrayData.length - 1].time3) {
                setCurrentRegister(<h1 className={classes.register}>Register your lunch break time entrance!</h1>)
            }
            else if (!arrayData[arrayData.length - 1].time4) {
                setCurrentRegister(<h1 className={classes.register}>Register your time exit!</h1>)
            }
            else if (DateHelper.isToday(new Date(formatedDate))) {
                setCurrentRegister(<h1 className={classes.register}>Register your arrive time!</h1>);            
            } 
            else{
                setCurrentRegister(<h1 className={classes.register}>See you tomorrow!</h1>);
            }
        }
        setRegisters(arrayData);
        setWarningMessage();

        setWeekResult(<h1 className={classes.result}>Accrued hours of the week: {result}</h1>)
    })
    }, [minTime])

    
    let date = new Date();
    let formatedDate = String(date.getMonth() + 1).padStart(2, '0') + '-' + String(date.getDate()).padStart(2, '0') + '-' + date.getFullYear();
    let today = String(date.getDate()).padStart(2, '0') + '/' + String(date.getMonth() + 1).padStart(2, '0') + '/' + date.getFullYear();
  
    const TimeRegisterHandler = () => {
        let time = timeValue.current.value;
        if (time) {
            Axios.get('/Timeregister.json')
            .then((response) => {
                const data = response.data;
                const arrayData = [];
                for(let register in data ){
                    if (data[register].day === formatedDate) {
                        urlPut = register
                        arrayData.push({
                            user: data[register].user,
                            day:data[register].day, 
                            dayweek: data[register].dayweek,
                            time1:data[register].time1, 
                            time2:data[register].time2,
                            time3:data[register].time3,
                            time4:data[register].time4,
                            accrued:data[register].accrued});
                    }
                }
                if (arrayData.length > 0){
                    if ((!arrayData[0].time2) && (arrayData[0].time1 !== time) && validateTime(time, arrayData[0].time1)){
                        //Exit hour number 1 (lunch)
                        timeRegisterData = {
                            ...arrayData[0],
                            time2: time
                        }
                        Axios.put('/Timeregister/' + urlPut + '.json', timeRegisterData).then(() => setMinTime(time))
                        lastTime = time;
                    }
                    else  if ((!arrayData[0].time3) && (arrayData[0].time2 !== time) && (arrayData[0].time2) && validateTime(time, arrayData[0].time2)){
                        //Enter hour number 2
                        timeRegisterData = {
                            ...arrayData[0],
                            time3: time
                        }
                        Axios.put('/Timeregister/' + urlPut + '.json', timeRegisterData).then(() => setMinTime(time))
                        lastTime = time;
                    }
                    else if ((!arrayData[0].time4) && (arrayData[0].time !== time) && (arrayData[0].time3) && validateTime(time, arrayData[0].time3)){
                       //Exit hour number 2
                        let horaFinal = getAccrued(arrayData[0].time1, arrayData[0].time2, arrayData[0].time3, time)
 
                        timeRegisterData = {
                            ...arrayData[0],
                            time4: time,
                            accrued: horaFinal
                        }
                        Axios.put('/Timeregister/' + urlPut + '.json', timeRegisterData).then(() => setMinTime(0))     
                        
                    }
                    else if (!validateTime(time, lastTime))
                    {
                        setWarningMessage(<h1>{"Put a time after the latest!"}</h1>);
                    }
                }
                else{
                    //Enter hour number 1
                    timeRegisterData = {
                        user:userContext.id,
                        day:formatedDate,
                        dayweek: daysOfWeek[date.getDay()],
                        time1: time
                    }
                    lastTime = time;
                    Axios.post('/Timeregister.json', timeRegisterData).then(() => setMinTime(time))
                }
            })
        }
    }

    return (
        <React.Fragment>
            <div>
                <h1 className={classes.date}>{today}</h1>
                <div className="container">
                    <div>
                        {currentRegister}
                        {warningMessage}
                    </div>
                    <div className={classes.ContentBox}>
                        <form className="form-group">
                            <div>
                                <input ref={timeValue} className='col-auto form-control'  type="time" required></input>
                                <button type="button" onClick={TimeRegisterHandler} className="btn btn-primary">Register</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div>
                {useMemo(() => <TimeRegister registers={registers}/>, [registers])}
            </div>
            <div className="container">
                {weekResult}
            </div>
        </React.Fragment>
    );
};

export default Register;