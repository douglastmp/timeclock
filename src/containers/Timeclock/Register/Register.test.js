import React from 'react'
import { configure, shallow } from 'enzyme'
import EnzymeAdapter from 'enzyme-adapter-react-16'
import Register from './Register'
import TimeRegister from '../../../components/TimeRegister/TimeRegister'

configure({adapter: new EnzymeAdapter()})

describe('<HistoricRegistes />', () => {
    const wrapper = shallow(<Register ></Register>);
    it("It should render the register page", 
    () => {
        expect(wrapper.find(TimeRegister));
    });

    it("It should render the register page", 
    () => {
        expect(wrapper.find("form"));
    });

    it("It should render the register page", 
    () => {
        expect(wrapper.find("h1"));
    });
 
}) 