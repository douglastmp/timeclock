import React, {Suspense} from 'react';
import { Route, NavLink, Switch, Redirect } from 'react-router-dom';
import Register from './Register/Register';
import Auth from '../auth/Auth'
import classes from './Timeclock.module.css';
import {connect} from 'react-redux'
import * as actionTypes from '../../store/actions'
import UserContext from './User/UserContext'
const  Historic = React.lazy(() => import('./Historic/Historic')); 

const timeclock = (props) => {
    let authButton = props.auth ? <NavLink 
                                    to="/"
                                    className="navbar-nav ml-auto"
                                    activeClassName=""
                                    onClick={props.onLogout}>
                                    Logout
                                    </NavLink>  : '' 

    return (
        <div className={classes.Timeclock}> 
            <header className={classes.Header}>
                <nav className="navbar nav-pills mr-auto justify-content-start">
                    <NavLink
                        className="nav-item nav-link "
                        to="/daily-register"
                        exact
                        activeClassName="active">
                        Register
                    </NavLink>
                    <NavLink 
                        className="nav-item nav-link "
                        to="/timeclock-historic" 
                        exact
                        activeClassName="active">
                        Historic
                    </NavLink>       
                    {authButton}
                </nav>  
            </header>   
            <Switch>
                <Route exact path="/daily-register" render={() => (props.auth ? <UserContext.Provider value={{id:props.userId}}><Register/></UserContext.Provider> : <Redirect to="/"/>) } />
                <Route exact path="/timeclock-historic" render={() => props.auth ? <UserContext.Provider value={{id:props.userId}}><Suspense fallback={<div>Loading...</div>}><Historic /></Suspense></UserContext.Provider> : <Redirect to="/"/>} />  
                <Route exact path="/" render={() => (props.auth ? <Redirect to="/daily-register"/> : <Auth />) } />
                <Route render={() => <h1>Not found</h1>}/>
            </Switch>        
        </div>
    );
}

const mapStateToProps = state => {
    return {
        auth: state.isAuth,
        userId: state.id
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onLogout: () => dispatch({type: actionTypes.LOGOUT})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(timeclock);