import React from 'react'
import classes from './HistoricRegisters.module.css'

const HistoricRegisters = (props) => {
    return (
    <div className="container">
        <div className={classes.HistoricRegisters}>
            <table className="table table-bordered table-hover" >
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>First Enter</th>
                        <th>Lunch Exit</th>
                        <th>Lunch Enter</th>
                        <th>Exit</th>
                        <th>Result</th>
                        <th>Accumulated</th>
                    </tr>
                </thead>
                <tbody>
                    {props.registers.map((singleRegister, index) => 
                        (<tr key={index}>
                            <td>{singleRegister.date}</td>
                            <td>{singleRegister.time1}</td>
                            <td>{singleRegister.time2}</td>
                            <td>{singleRegister.time3}</td>
                            <td>{singleRegister.time4}</td>
                            <td>{singleRegister.accrued}</td>
                            <td>{singleRegister.accumulated}</td>
                        </tr>)
                    )}  
                </tbody>
            </table>
        </div>
    </div>
    )
}

export default HistoricRegisters;