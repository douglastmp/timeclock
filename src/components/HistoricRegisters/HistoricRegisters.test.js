import React from 'react'
import { configure, shallow } from 'enzyme'
import EnzymeAdapter from 'enzyme-adapter-react-16'
import HistoricRegistes from './HistoricRegisters'

configure({adapter: new EnzymeAdapter()})

describe('<HistoricRegistes />', () => {
    it("It should render a table", 
    () => {
        const wrapper = shallow(<HistoricRegistes registers={[]}></HistoricRegistes>);
        expect(wrapper.find("table"))
    });
}) 