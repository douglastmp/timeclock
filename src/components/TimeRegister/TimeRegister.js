import React from 'react';

const TimeRegister = (props) => {
    return(
        <div className="container">
            <table className="table table-bordered table-hover" >
                <thead>
                    <tr>
                        <th>Day of Week</th>
                        <th>First Enter</th>
                        <th>Lunch Exit</th>
                        <th>Lunch Enter</th>
                        <th>Exit</th>
                        <th>Accrued</th>
                    </tr>
                </thead>
                <tbody>
                    {props.registers.map((singleRegister, index) => 
                        (<tr key={index}>
                            <td>{singleRegister.day}</td>
                            <td>{singleRegister.time1}</td>
                            <td>{singleRegister.time2}</td>
                            <td>{singleRegister.time3}</td>
                            <td>{singleRegister.time4}</td>
                            <td>{singleRegister.accrued}</td>
                        </tr>)
                    )}  
                </tbody>
            </table>
        </div>
    );
}

export default TimeRegister;