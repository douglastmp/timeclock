import React from 'react'
import { configure, shallow } from 'enzyme'
import EnzymeAdapter from 'enzyme-adapter-react-16'
import TimeRegister from './TimeRegister'

configure({adapter: new EnzymeAdapter()})

describe('<TimeRegister />', () => {
    it("It should render a table", 
    () => {
        const wrapper = shallow(<TimeRegister registers={[]}></TimeRegister>);
        expect(wrapper.find("table"))
    });
}) 