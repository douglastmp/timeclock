This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Timeclock Project

App with the object to register the worked hours of the employees 

You can also see the project at the link: 
https://timeclock-a4b2e.firebaseapp.com/

## Pages
--Auth
Page for authenticate a user, registration is not allowd, for test use this user: 
username: douglas
password: auth@132

--Register
The main app is divide in 2 pages, the first one called "Register" is where the employee can 
register the arriving and exiting hours, including lunch break. The system will show a 
message informing which  hour is the correspondent to put. Below that, the system will 
show all registered hours of the week, giving the sum of hours accumulated, (being positive or negative)
for the day and the result of the week.

--Historic
The second page shows a full report of the worked hours, which  can be filtered by a specific
date. This page also shows the result for each day and the final result for the chosen period.
In case of empty filters the page will show the result of the correspondent month.

## Available Scripts
In the project directory, you can run:

### `npm install`
Runs the instalation of the package.json dependencies


### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
